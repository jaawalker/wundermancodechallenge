//
//  BookCell.swift
//  WundermanCodeChallenge
//
//  Created by Daniel Lyon on 1/16/20.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    var book: Book? {
        didSet {
            titleLabel.text =  book?.title ?? String()
            authorLabel.text =   book?.author ?? String()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        book = nil
    }
    
    override func prepareForReuse() {
        coverImageView?.image = nil
        book = nil
    }
}
