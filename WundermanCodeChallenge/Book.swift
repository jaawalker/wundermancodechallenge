//
//  Book.swift
//  WundermanCodeChallenge
//
//  Created by Daniel Lyon on 1/16/20.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import Foundation

// MARK: - Book
struct Book: Codable {
    
    let title: String?
    let imageURL: String?
    let author: String?
}
