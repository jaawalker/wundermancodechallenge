//
//  ViewController.swift
//  WundermanCodeChallenge
//
//  Created by Daniel Lyon on 1/16/20.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import UIKit

/// initial VC
class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let booksURL = "https://de-coding-test.s3.amazonaws.com/books.json"
    let cellReuseId = "cell"
    let defaultCoverImage = #imageLiteral(resourceName: "defaultCover")
    
    var books: [Book]? {
        didSet {
            refreshTable()
        }
    }
    
    // fake cache
    let imageCache = NSCache<NSString, UIImage>()
}

// Life cycle 
extension ViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchBooks()
        addRefreshControll()
    }
}

// refresh controll
extension ViewController {
    
    func addRefreshControll() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshBooks(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    /// refreshes list of  books
    @objc func refreshBooks(_ sender: UIRefreshControl) {
        fetchBooks() {
            DispatchQueue.main.async {
                sender.endRefreshing()
            }
        }
    }
}

// refresh table
extension ViewController {
    
    func refreshTable(_ animated: Bool = false) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

// tableview UITableViewDelegate UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId, for: indexPath) as? BookCell else {
            return UITableViewCell()
        }
        configure(cell, for: indexPath)
        return cell
    }
}

// configure cell
extension ViewController {
    
    func configure(_ cell: BookCell, for indexPath: IndexPath) {
        guard let books = books else { return }
        let book = books[indexPath.row]
        cell.book = book
        setImage(for: book, at: indexPath) { image in
            DispatchQueue.main.async {
                cell.coverImageView?.image = image ?? self.defaultCoverImage
            }
        }
    }
    
    func setImage(for book: Book, at indexPath: IndexPath, _ completion: @escaping (_ image: UIImage?)-> ()) {
        guard let urlString = book.imageURL, let imageURL = URL(string: urlString) else {
            completion(nil)
            return
        }
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            completion(cachedImage)
        } else {
            downloadImage(from: imageURL, completion: { image in
                completion(image)
            })
        }
    }
}

// Networking
extension ViewController {
    
    /**
     fetchs list of books from server
     - Parameter completion: closure optional completionHandler nil by default available.
     */
    func fetchBooks(completion: (() -> ())? = nil) {
        let url = URL(string: booksURL)!
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { (data, response, error) in
            guard let data = data else {
                return
            }
            do {
                let decoder = JSONDecoder()
                let books = try decoder.decode([Book].self, from: data)
                self.books = books
                completion?()
            }
            catch let error as NSError {
                print("error:\(error)")
            }
        }
        task .resume()
    }
    
    /**
     downloads Image from server
     - Parameter url: URL  to use for network request
     - Parameter completion: completion handler
     - Parameter image: optional UIImage used in completionHandler
     */
    func downloadImage(from url: URL, completion: @escaping (_ image: UIImage?)-> ()) {
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { (data, response, error) in
            var image: UIImage?
            if let data = data {
                image = UIImage(data: data) ?? self.defaultCoverImage
                self.imageCache.setObject(image!, forKey: url.absoluteString as NSString)
            }
            completion(image)
        }
        task.resume()
    }
}
