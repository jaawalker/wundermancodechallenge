## Wunderman Code Challenge
#### Directions Received

* URL from which to fetch books data: http://de-coding-test.s3.amazonaws.com/books.json
* Take no more than two hours to complete and submit your project.
* Impress us! If you finish early, consider adding features and cleaning up your code. Your final submission within the two-hour window is the one that counts.
* Even if you’re not finished, submit what you have.

## To Review Exercise:

- git clone https://bitbucket.org/jaawalker/wundermancodechallenge/src/master/
* cd ~/Path/To/wundermancodechallenge
* Be amazed!

### Thanks!
```
Thanks for taking the time to review this. If you have any questions please reach out.
Daniel Lyon
``` 
-
